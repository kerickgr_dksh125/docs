# Список источников

*Note:* Список будет пополняться.

## Практика

- **Курс в Autocode**:\
[https://autocode.lab.epam.com/student/group/235](https://autocode.lab.epam.com/student/group/235)

## Рекомендуемая литература
- В. Спрингер - **Гид по Computer Science**\
Тонкая универсально полезная книжка. Рассказано об оценке сложности алгоритмов, самих алгоритмов и структур данных, а также приемах их проектирования.

- К. Хорстманн - **Java. Библиотека профессионала.** 
    - Том 1. Основы
    - Том 2. Расширенные средства программирования\
Классический здоровенный дорогой двухтомник по Java.
Очень большой, подробный и мой любимый.
Описано почти все, что вам может понадобиться из Java SE.

## Дополнительные материалы

### git
- S. Chacon, B. Straub. **Pro Git.**\
    Основная книга по Git. Распространяется бесплатно в электронном виде. Купить напечатанную версию тоже, конечно, можно. 
    - Оригинал: [https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)
    - Перевод: [https://git-scm.com/book/ru/v2](https://git-scm.com/book/ru/v2)
- **Онлайн курс по Git от EPAM**: [https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e](https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e)
- **Онлайн курс по Git от devcolibri**: [https://devcolibri.com/course/git-для-начинающих/](https://devcolibri.com/course/git-%D0%B4%D0%BB%D1%8F-%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D1%8E%D1%89%D0%B8%D1%85/)
- **Вводная статья по Git от JRebel + CheatSheet**: [https://www.jrebel.com/blog/git-cheat-sheet](https://www.jrebel.com/blog/git-cheat-sheet)
- **Git CheetSheet от GitHub**:[https://training.github.com/downloads/ru/github-git-cheat-sheet/](https://training.github.com/downloads/ru/github-git-cheat-sheet/)
- **Интерактивный Git CheetSheet**: [https://ndpsoftware.com/git-cheatsheet.html](https://ndpsoftware.com/git-cheatsheet.html)
- Перевод старой известной статьи **A successful Git branching model**, описывающей Git Flow: [https://habr.com/ru/post/106912/](https://habr.com/ru/post/106912/)
- **Learning Git Branching**: [https://learngitbranching.js.org/](https://learngitbranching.js.org/) \
    Интерактивный тренажер по освоению ветвления в git

### Collections. Generics
- **Итератор как паттерн**. Refactoring.guru: https://refactoring.guru/ru/design-patterns/iterator
- Справочник по **Java Collections Framework**. Хабр: https://habr.com/ru/post/237043/
- Пришел, увидел, обобщил: погружаемся в **Java Generics**. Хабр: https://habr.com/ru/company/sberbank/blog/416413/

### Maven
- Apache Maven Project. **Maven in 5 minutes**: [https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)\
    Микрогайд от разработчиков
- Apache **Maven Tutorial. Baeldung**: [https://www.baeldung.com/maven](https://www.baeldung.com/maven)\
    Микрогайд от мастеров микрогайдов
- **Maven Tutorial. Jenkov**: [http://tutorials.jenkov.com/maven/maven-tutorial.html](http://tutorials.jenkov.com/maven/maven-tutorial.html)\
    Гайд от серьезного человека, с подробностями
- Chad Darby, **Maven tutorial** Youtube Playlist: [https://www.youtube.com/watch?v=Fe6lrsPmseo&list=PLEAQNNR8IlB7uvr8EJbCNJq2I82A8cqE7](https://www.youtube.com/watch?v=Fe6lrsPmseo&list=PLEAQNNR8IlB7uvr8EJbCNJq2I82A8cqE7)\
    Видеогайд от улыбчивого человека (подробностей поменьше)

### JUnit
- **JUnit 4 Wiki**: [https://github.com/junit-team/junit4/wiki](https://github.com/junit-team/junit4/wiki)
- **JUnit 4 Parameterized**: [https://www.tutorialspoint.com/junit/junit_parameterized_test.htm](https://www.tutorialspoint.com/junit/junit_parameterized_test.htm)
- **JUnit 5 User Guide**: [https://junit.org/junit5/docs/current/user-guide/](https://junit.org/junit5/docs/current/user-guide/)
- **JUnit 5 Baeldung Guide**: [https://www.baeldung.com/junit-5](https://www.baeldung.com/junit-5)
- **JUnit 5 Parameterized Tests**: [https://www.baeldung.com/parameterized-tests-junit-5](https://www.baeldung.com/parameterized-tests-junit-5)

### Lambdas. Streams.
- **Курс по лямбдам и стримам в Java.**\
    Ни секунды видео, один лишь текст и примеры. Задачки тоже есть.\
    [https://stepik.org/course/1595](https://stepik.org/course/1595)
- **Подробное руководство по стримам на русском с хорошей визуализацией**.\
    [https://annimon.com/article/2778](https://annimon.com/article/2778)
- **О стримах от их разработчика**
    - Часть 1. https://www.youtube.com/watch?v=O8oN4KSZEXE
    - Часть 2. https://www.youtube.com/watch?v=i0Jr2l3jrDA
- **О задачах и стримах от Тагира**. Он разбирается.\
    [https://www.youtube.com/watch?v=vxikpWnnnCU](https://www.youtube.com/watch?v=vxikpWnnnCU)

### I/O
- **Java IO Tutorial** (Jenkov): [http://tutorials.jenkov.com/java-io/index.html](http://tutorials.jenkov.com/java-io/index.html)
- **Java NIO Tutorial** (Jenkov, veeeery optional): [http://tutorials.jenkov.com/java-nio/index.html](http://tutorials.jenkov.com/java-nio/index.html)
- **Java NIO.2 Introduction** (Baeldung): [https://www.baeldung.com/java-nio-2-file-api](https://www.baeldung.com/java-nio-2-file-api)
