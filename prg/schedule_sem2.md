# Календарный план "Программирование" (Семестр 2)

### Занятия

| # | Date | Topic |
| --- | --- |--- |
| 1 | 18.02 | Вводная. Big O |
| 2 | 25.02 | Collections |
| 3 | 04.03 | Maven. Unit tests. Debugging. |
| 4 | 11.03 | Oblivion |
| 5 | 18.03 | Lambdas. Streams |
| 6 | 25.03 | IO. Files |
| 7 | 01.04 | Threads. Mutex. Synchronizers. |
| 8 | 08.04 | ExecutorServices. Future. CompletableFuture.|
| 9 | 15.04 | SQL+JDBC |
| 10 | 22.04 | Java UI. JavaFX? Swing? |
| 11 | 29.04 | TBA |
| 12 | 06.05 | TBA |
| 13 | 13.05 | TBA |
| 14 | 20.05 | TBA |
| 15 | 27.05 | TBA | 

### Autocode Deadlines

| Блок упражнений | Мягкий Дедлайн | Дедлайн |
| --- | --- | --- |
| Meet Autocode| 11.03  | 31.03  |
| Java Fundamentals | 17.03  | 07.04 |
| Java Collections | 24.03  | 14.04 |
| JUnit | 31.03  | 21.04 |
| Java Streams | TBA  | TBA |
| IO | TBA  | TBA |
| Concurrency | TBA  | TBA |
| Java + SQL. JDBC | TBA  | TBA |

### Project Stages

| Этап | Мягкий Дедлайн | Дедлайн |
| --- | --- | --- |
| Подача заявки. High-Level Overview | TBA  | TBA  |
| Проектирование. Design Document. | TBA  | TBA |
| Реализация. Project overview. Demo. | TBA  | TBA |

- Если не успеть в **Мягкий Дедлайн**, применяется понижающий коэффициент **0,8**
- Если не успеть в **Дедлайн**, применяется понижающий коэффициент **0,6**
