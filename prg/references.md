# Список источников

*Note:* Список будет пополняться.

## Практика

- Класс в курсе **"Введение в программирование (C++)"**\
[https://stepik.org/join-class/2ab340cd5ee0a0ac698301a0d8c988b998ee217d](https://stepik.org/join-class/2ab340cd5ee0a0ac698301a0d8c988b998ee217d)
- **Курс в Autocode по ООП**:\
[https://autocode.lab.epam.com/student/group/109](https://autocode.lab.epam.com/student/group/109)

## Рекомендуемая литература
- В. Спрингер - **Гид по Computer Science**\
Тонкая универсально полезная книжка. Рассказано об оценке сложности алгоритмов, самих алгоритмов и структур данных, а также приемах их проектирования.

- Б. Керниган, Д. Ритчи - **Язык программирования С**\
Книга по основам Си (не плюсов) и, следовательно, структурному программированию.
Про специфические для языка фишки вроде указателей и функций управления памятью тоже рассказано.

- Б. Керниган, Р. Пайк - **Практика программирования**\
Книга рассказывает о концепциях и приемах для решения задач, которые часто возникают в практике программирования.
Например, рассказывают и об оценке сложности алгоритма и, например, и о концепции интерфейсов.
Примеры даны разных языках с обзором их особенностей.

- Б. Страуструп - **Дизайн и эволюция языка С++**\
Если плюсы поселились в вашем сердце, то эта книга - для вас.
Создатель языка расскажет, какие решения в языке есть и почему так было сделано.

- К. Хорстманн - **Java. Библиотека профессионала.** 
    - Том 1. Основы
    - Том 2. Расширенные средства программирования\
Классический здоровенный дорогой двухтомник по Java.
Очень большой, подробный и мой любимый.
Описано почти все, что вам может понадобиться из Java SE.

## Дополнительные материалы

### С++ in-depth
- **Программирование на языке C++** - [https://stepik.org/course/7](https://stepik.org/course/7)\
Курс достаточно глубоко погружает в особенности программирования именно на плюсах и дает много теории, в том числе и о реализации инструментов ООП.
Практика тоже есть, и она непростая, хотя ее и немного.

### Frontend
- Д. Дакетт - **Javascript и jQuery. Интерактивная веб-разработка**
- Д. Дакетт - **HTML и CSS. Разработка и дизайн веб-сайтов**
Это не очень большие книги по фронтенду, выделяющиеся продуманным оформлением.
Для фронтенда чувство визуального стиля, пожалуй, важно. Уточню, что книги охватывают именно основы, так что рекомендовать их могу в основном именно новичкам.

### git
- S. Chacon, B. Straub. **Pro Git.**\
    Основная книга по Git. Распространяется бесплатно в электронном виде. Купить напечатанную версию тоже, конечно, можно. 
    - Оригинал: [https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)
    - Перевод: [https://git-scm.com/book/ru/v2](https://git-scm.com/book/ru/v2)
- **Онлайн курс по Git от EPAM**: [https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e](https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e)
- **Онлайн курс по Git от devcolibri**: [https://devcolibri.com/course/git-для-начинающих/](https://devcolibri.com/course/git-%D0%B4%D0%BB%D1%8F-%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D1%8E%D1%89%D0%B8%D1%85/)
- **Вводная статья по Git от JRebel + CheatSheet**: [https://www.jrebel.com/blog/git-cheat-sheet](https://www.jrebel.com/blog/git-cheat-sheet)
- **Git CheetSheet от GitHub**:[https://training.github.com/downloads/ru/github-git-cheat-sheet/](https://training.github.com/downloads/ru/github-git-cheat-sheet/)
- **Интерактивный Git CheetSheet**: [https://ndpsoftware.com/git-cheatsheet.html](https://ndpsoftware.com/git-cheatsheet.html)
- Перевод старой известной статьи **A successful Git branching model**, описывающей Git Flow: [https://habr.com/ru/post/106912/](https://habr.com/ru/post/106912/)
- **Learning Git Branching**: [https://learngitbranching.js.org/](https://learngitbranching.js.org/) \
    Интерактивный тренажер по освоению ветвления в git
    
---
### К лекции 10.09
- **Ликбез по типизации в языках программирования** [https://habr.com/ru/post/161205/](https://habr.com/ru/post/161205/)\
Отсюда брал структуру для содержательной половины лекции

---
### Материалы 07.12
Это материалы, которые можно изучить взамен пропущенных заданий.
#### Обязательные (по ним есть задачи в Автокоде)
- Stepik. **Базовый курс по Java.**
    - 3.5 Абстрактные классы и интерфейсы: https://stepik.org/lesson/14513/step/1?unit=4147 \
    Здесь нужно посмотреть о том, что такое абстрактные классы и интерфейсы - важные инструменты ООП в Java.
    - 4.1 Знакомство с исключениями: https://stepik.org/lesson/12772/step/1?unit=3120
    - 4.2 Обработка исключений. Try-catch: https://stepik.org/lesson/12772/step/1?unit=3121 \
    В этих разделах нужно посмотреть, что такое исключения и как их обрабатывать
#### Для интересующихся, необязательные
- Stepik. **Базовый курс по Java**: https://stepik.org/course/187/syllabus \
Разделы, не входящие в обязательные, помогут вспомнить, что мы обсуждали в крэш сессии про основы Java.
    - 1. Введение в Java
    - 2. Базовый синтаксис Java
    - 3. Объекты, классы и пакеты в Java
    - 4. Обработка ошибок, исключения, отладка
- Stepik. **Программирование на языке C++**: https://stepik.org/course/7/syllabus \
Первые два раздела напомнят о том, как работают плюсы.
Третий расскажет о классах в плюсах, и какие есть отличия.
Четвертый расскажет об особенностях ООП в плюсах, вы увидите, какие есть отличия от Java в наследовании, что такое виртуальные методы (в Java, например, все методы виртуальные), поймете, что в плюсах вместо интерфейсов, почему и как работает множественное наследование, и какие с этим есть проблемы.
    - 1. Введение в язык C++
    - 2. Как выполняются программы на C++
    - 3. Структуры и классы
    - 4. Объектно-ориентированное программирование

### Материалы 21.12
Это материалы к занятию по коллекциям.
- **Итератор как паттерн**. Refactoring.guru: https://refactoring.guru/ru/design-patterns/iterator
- Справочник по **Java Collections Framework**. Хабр: https://habr.com/ru/post/237043/
- Пришел, увидел, обобщил: погружаемся в **Java Generics**. Хабр: https://habr.com/ru/company/sberbank/blog/416413/


### Материалы 28.12
Это материалы к занятию по SOLID и коллекциям.
- **SOLID** (Хабр): https://habr.com/ru/post/348286/
- **Шаблоны проектирования**. Refactoring.guru: https://refactoring.guru/ru/design-patterns
- **Приёмы объектно-ориентированного проектирования. Паттерны проектирования** Информация о книге: https://ru.wikipedia.org/wiki/Design_Patterns
- *по запросу* **Regular expressions in Java** - Tutorial (Vogella) https://www.vogella.com/tutorials/JavaRegularExpressions/article.html
